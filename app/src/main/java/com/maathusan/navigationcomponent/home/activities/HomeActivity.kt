package com.maathusan.navigationcomponent.home.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.maathusan.navigationcomponent.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        supportActionBar?.setDisplayHomeAsUpEnabled(true);
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}