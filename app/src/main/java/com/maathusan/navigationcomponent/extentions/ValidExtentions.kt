package com.maathusan.navigationcomponent.extentions

import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.isValidEmail(): Boolean {
    var valid: Boolean
    try {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN =
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"

        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(this)

        valid = matcher.matches()
    } catch (e: Exception) {
        valid = false
        e.printStackTrace()
    }
    return valid
}

fun String.isValidName(): Boolean {
    var valid: Boolean
    try {
        val pattern: Pattern
        val matcher: Matcher
        val PATTERN = "^[A-Za-z-\\s']*+"

        pattern = Pattern.compile(PATTERN)
        matcher = pattern.matcher(this)

        valid = matcher.matches()
    } catch (e: Exception) {
        valid = false
        e.printStackTrace()
    }
    return valid && this.isNotEmpty()
}

fun String.isValidPassword(): Boolean {
    var valid: Boolean
    try {
        valid = this.length >= 6
    } catch (e: Exception) {
        valid = false
        e.printStackTrace()
    }

    return valid
}
