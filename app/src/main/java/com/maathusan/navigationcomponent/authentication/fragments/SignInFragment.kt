package com.maathusan.navigationcomponent.authentication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.maathusan.navigationcomponent.R
import com.maathusan.navigationcomponent.extentions.isValidEmail
import com.maathusan.navigationcomponent.extentions.isValidPassword
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*

class SignInFragment : Fragment(),View.OnClickListener {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        view.btn_sign_in.setOnClickListener(this)
        view.btn_sign_up.setOnClickListener(this)
        view.btn_forgot.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v!!.id){
            // Navigate to Home activity from sign in fragment
            R.id.btn_sign_in -> {
                if (isValid()){
                    activity?.finish().also {
                        navController!!.navigate(R.id.action_signInFragment_to_homeActivity)
                    }
                }


            }
            // Navigate to sign up fragment from sign in fragment
            R.id.btn_sign_up -> navController!!.navigate(R.id.action_signInFragment_to_signUpFragment)

            // Navigate to forgot password fragment from sign in fragment
            R.id.btn_forgot -> navController!!.navigate(R.id.action_signInFragment_to_forgotPasswordFragment)

        }
    }

    //Check Validation
    fun isValid():Boolean{
        var valid: Boolean

        if (!et_email.text.toString().isValidEmail()){
            valid = false
        }else valid = et_password.text.toString().isValidPassword()


        return valid
    }

}