package com.maathusan.navigationcomponent.authentication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.maathusan.navigationcomponent.R
import com.maathusan.navigationcomponent.extentions.isValidEmail
import com.maathusan.navigationcomponent.extentions.isValidName
import com.maathusan.navigationcomponent.extentions.isValidPassword
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.btn_sign_in
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.*


class SignUpFragment : Fragment(),View.OnClickListener {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        view.btn_new_signIn.setOnClickListener(this)
        view.btn_new_signup.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            // Navigate to home activity from sign up fragment
            R.id.btn_new_signup -> {
                if (isValid()){
                    activity?.finish().also {
                        navController!!.navigate(R.id.action_signUpFragment_to_homeActivity)
                    }
                }


            }
            // Navigate to back from sign up fragment
            R.id.btn_new_signIn -> requireActivity().onBackPressed()
        }
    }

    //Check Validation
    fun isValid():Boolean{
        var valid: Boolean

        if (!et_name.text.toString().isValidName()){
            valid = false
        }else if (!et_enter_email.text.toString().isValidEmail()){
            valid = false
        }else if (!et_enter_password.text.toString().isValidPassword()){
            valid = false
        }else valid = et_enter_confirm.text.toString() == et_enter_password.text.toString()



        return valid
    }

}